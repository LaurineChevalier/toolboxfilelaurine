---
title: "get_one_file_size.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```


# get_one_file_size
    
```{r function-get_one_file_size}
#' Fonction qui renvoie un data.frame avec son nom et sa taille 
#' 
#'
#' Cette fonction prend un nom de fichier en paramètre, et renvoie un data.frame avec son nom et sa taille
#' @param file Un dataframe 
#'
#'
#' @return dataframe
#' 
#' @export
#' 

get_one_file_size <- function(file){
   if(isFALSE(is.character(file))) {
    stop("file is not a character vector")
   } else {
      result <- data.frame(name=file,size=file.size(file))  
  return(result)
  }

}
```
  
```{r example-get_one_file_size}
get_one_file_size(file = "0-dev_history.Rmd" )
```
  
```{r tests-get_one_file_size}
test_that("get_one_file_size works", {
  expect_true(inherits(get_one_file_size, "function")) 
  expect_error(object = get_one_file_size(1), regexp = "file is not a character vector")
})
```
  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_get_one_file_size.Rmd", 
               vignette_name = "get_one_file_size")
```
